import { Component, OnInit } from '@angular/core';
import { EncounterService } from '../encounter.service';

@Component({
  selector: 'app-view-encounters',
  templateUrl: './view-encounters.component.html',
  styleUrls: ['./view-encounters.component.css']
})
export class ViewEncountersComponent implements OnInit {

  constructor(private encs: EncounterService) { }

  ngOnInit() {
  }

  getEncounters() {
    return this.encs.encounters;
  }

  runEncounter(index){
    
  }

}
