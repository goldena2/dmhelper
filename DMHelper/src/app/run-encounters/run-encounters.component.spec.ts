import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunEncountersComponent } from './run-encounters.component';

describe('RunEncountersComponent', () => {
  let component: RunEncountersComponent;
  let fixture: ComponentFixture<RunEncountersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunEncountersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunEncountersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
