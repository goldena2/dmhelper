import { Player } from './player';
import { Monster } from './monster';
export class Enounter {
    name: string;
    note: string;
    monsters: string[];
    players: Player[];

    constructor(name){
        this.name = name;
        this.monsters = [];
    }
    addPlayers(players) {
        this.players = [];
        players.forEach(name => {
            this.players.push(new Player(name));
        });
    }
}
