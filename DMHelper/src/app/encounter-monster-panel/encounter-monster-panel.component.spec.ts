import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncounterMonsterPanelComponent } from './encounter-monster-panel.component';

describe('EncounterMonsterPanelComponent', () => {
  let component: EncounterMonsterPanelComponent;
  let fixture: ComponentFixture<EncounterMonsterPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncounterMonsterPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncounterMonsterPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
