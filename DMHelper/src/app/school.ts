export class School {
    name: string;
    url: string;
    constructor(data) {
        this.name = data.name;
        this.url = data.url;
    }
}
