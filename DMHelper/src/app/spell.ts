import { Component } from '@angular/core';
import { School } from './school';
import { Class } from './class';

export class Spell {
    castingTime: string;
    classes: Class[];
    components: string[];
    concentration: boolean;
    material: string;
    desc: string[];
    duration: string;
    level: number;
    name: string;
    range: string;
    school: School;
    id: string;
    ritual: boolean;
    higherLevels: string[];

    create(data) {
        this.duration = data.duration;
        this.higherLevels = data.higher_level;
        this.id = data._id;
        this.castingTime = data.casting_time;
        this.components = data.components;
        this.material = data.material;
        this.desc = data.desc;
        this.name = data.name;
        this.concentration = data.concentration === 'yes' ? true : false;
        this.ritual = data.ritual === 'yes' ? true : false;
        this.level = data.level;
        this.range = data.range;
        this.classes = [];
        this.getClasses(data.classes);
        this.school = new School(data.school);
    }

    getClasses(data: object[]) {
        data.forEach(element => {
            this.classes.push(new Class(element['name'], element['url']));
        });

    }
}

