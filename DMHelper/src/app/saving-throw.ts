export class SavingThrow {
    name: string;
    bonus: number;
    generateThrows(data: object){
        let ret = [];
        const savelist = [
            'constitution_save',
            'intelligence_save',
            'wisdom_save',
            'strength_save',
            'dexterity_save',
            'charisma_save',
        ];
        savelist.forEach(element => {
            if(data[element]) {
                let temp = new SavingThrow();
                temp.name = element;
                temp.bonus = data[element];
                ret.push(temp);
            }
        });
        return ret;
    }
}
