import { SpellsService } from './../spells.service';
import { ActivatedRoute } from '@angular/router';
import { Spell } from './../spell';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-spell-view',
  templateUrl: './spell-view.component.html',
  styleUrls: ['./spell-view.component.css']
})
export class SpellViewComponent implements OnInit {
  id: string;
  spell: Spell;
  constructor(private route: ActivatedRoute,  spells: SpellsService){
    this.route.params.subscribe( params => {
      this.id = params['id'];
      this.spell =  spells.getSpellById(this.id);
    });
  }

  ngOnInit() {
  }

}
