import { MonstersService } from './../monsters.service';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import{ Monster } from '../monster';
import { EncounterService } from '../encounter.service';
@Component({
  selector: 'app-monster-view',
  templateUrl: './monster-view.component.html',
  styleUrls: ['./monster-view.component.css']
})
export class MonsterViewComponent implements OnInit {
  id: string;
  monster: Monster;
  constructor(private encs: EncounterService, private route: ActivatedRoute, private monsterService: MonstersService) {
    this.monster =  this.monsterService.getMonsterById(this.id);
  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.id = params['id'];
      this.monster = this.monsterService.getMonsterById(this.id);
      if (!this.monster) {
        this.monsterService.getMonsters();
      }
   });
  }

  getIndex(type: string){
    let num = Math.floor(this.monster[type].length / 2);
    if (this.monster[type].length % 2 !== 0) {
      num++;
    }
    const ret = [];
    for (let i = 0; i < num; i += 2){
      ret.push(i);
    }
    return ret;
  }

  getEncounternames() {
    return this.encs.getNames();
  }

  addMonster(i, amount) {
    this.encs.addMonster(i, amount, this.id);
  }

}
