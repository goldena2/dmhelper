import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { Monster } from '../monster';
import { MonstersService } from '../monsters.service';
import { element } from 'protractor';

@Component({
  selector: 'app-all-monsters',
  templateUrl: './all-monsters.component.html',
  styleUrls: ['./all-monsters.component.css']
})
export class AllMonstersComponent implements OnInit {
  monsters: Monster[];
  filtered: Monster[];
  constructor(private monsterSevice: MonstersService) {
    if(!this.monsters){
      this.monsters = monsterSevice.getAllMonsters();
      this.filtered = this.monsters;
    }
   }

  ngOnInit() {
    this.monsterSevice.getMonsters();
  }

  getMonster(): Monster{
    return new Monster();
  }
  filterMonsters(cr: string, type, alignment, size, name) {
    type = type.toLowerCase();
    let numCr: number;
    if(cr === ''){
      this.filtered = this.monsters;
    }else{
      if(cr !== '0'  && cr.split('/').length > 1){
        switch(cr){
          case('1/8'):
            numCr = .125;
            break;
          case('1/4'):
            numCr = .25;
            break;
          case('1/2'):
            numCr = .5;
        }
      }else{
        numCr = +cr;
      }
    }
    this.filtered = this.monsters.filter(monster => {
        if ((numCr === monster['cr'] || (!cr)) &&
        (type === monster['type'] || (!type)) &&
        (alignment === monster['alignment'] || !alignment) &&
        (size === monster['size'] || (!size)) &&
        (monster['name'].toLocaleLowerCase().includes(name.toLocaleLowerCase()) || (!name)))
        {
          return true;
        }else{
          return false;
        }
      });
  }

  getIndexes(){
    let number = Math.floor(this.filtered.length / 3);
    if(this.filtered.length%3 != 0){
      number++;
    }
    let ret = [];
    let test = 0;
    for(var i = 0; i < number; i++){
      ret.push(test);
      test += 3;
    }
    return ret;
  }
}
